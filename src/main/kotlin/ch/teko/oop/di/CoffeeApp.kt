package ch.teko.oop.di

import ch.teko.oop.di.coffee.maker.CoffeeMaker
import ch.teko.oop.di.coffee.coffeeAppModule
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject

class CoffeeApp : KoinComponent {

    val coffeMaker: CoffeeMaker by inject()

    fun run() {
        coffeMaker.brew()
    }
}

fun main() {
    startKoin {
        modules(coffeeAppModule)
    }

    CoffeeApp().run()
    CoffeeApp().run()
}