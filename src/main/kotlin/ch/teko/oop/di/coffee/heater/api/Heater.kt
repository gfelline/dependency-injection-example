package ch.teko.oop.di.coffee.heater.api

interface Heater {

    fun on()
    fun off()
    fun isHot() : Boolean

}