package ch.teko.oop.di.coffee.pump.api

interface Pump {
    fun pump()
}