package ch.teko.oop.di.coffee.pump.impl

import ch.teko.oop.di.coffee.heater.api.Heater
import ch.teko.oop.di.coffee.pump.api.Pump

class Thermosiphon(val heater: Heater) : Pump {
    override fun pump() {
        if (heater.isHot()) {
            println("=> => pumping => =>")
        }
    }
}