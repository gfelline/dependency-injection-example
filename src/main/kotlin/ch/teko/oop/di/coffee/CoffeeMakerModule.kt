package ch.teko.oop.di.coffee

import ch.teko.oop.di.coffee.heater.api.Heater
import ch.teko.oop.di.coffee.heater.impl.SuperElectricHeater
import ch.teko.oop.di.coffee.maker.CoffeeMaker
import ch.teko.oop.di.coffee.pump.api.Pump
import ch.teko.oop.di.coffee.pump.impl.Thermosiphon
import org.koin.dsl.module

val coffeeAppModule = module {
    factory { CoffeeMaker(get(), get()) }
    single<Pump> { Thermosiphon(get()) }
    single<Heater> { SuperElectricHeater() }
}