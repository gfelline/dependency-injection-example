package ch.teko.oop.di.coffee.heater.impl

import ch.teko.oop.di.coffee.heater.api.Heater

class ElectricHeater : Heater {

    var heating = false

    override fun on() {
        println("~ ~ ~ heating ~ ~ ~ ")
        heating = true
    }

    override fun off() {
        heating = false
    }

    override fun isHot(): Boolean = heating
}