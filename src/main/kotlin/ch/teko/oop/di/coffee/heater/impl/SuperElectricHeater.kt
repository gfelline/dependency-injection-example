package ch.teko.oop.di.coffee.heater.impl

import ch.teko.oop.di.coffee.heater.api.Heater
import java.util.*

class SuperElectricHeater : Heater {

    lateinit var id:String

    init {
        id = UUID.randomUUID().toString()
    }

    var heating = false

    override fun on() {
        println("~ ~ ~ super heating ~ ~ ~ ")
        println("UUID: $id")
        heating = true
    }

    override fun off() {
        heating = false
    }

    override fun isHot(): Boolean = heating
}