package ch.teko.oop.di.coffee.maker

import ch.teko.oop.di.coffee.heater.api.Heater
import ch.teko.oop.di.coffee.pump.api.Pump

class CoffeeMaker(val pump: Pump, val heater: Heater) {

    fun brew() {
        heater.on()
        pump.pump()
        println("[_]P coffe! [_]P")
    }

}